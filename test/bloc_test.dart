import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_covidapp_bloc_test/bloc/bloc.dart';
import 'package:flutter_covidapp_bloc_test/data/api_service.dart';
import 'package:flutter_covidapp_bloc_test/data/covid_repository.dart';
import 'package:flutter_covidapp_bloc_test/data/models/global.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockCovidRepository extends Mock implements Repository {}

void main() {
  MockCovidRepository mockCovidRepository = MockCovidRepository();

  setUp(() {
    //mockCovidRepository = MockCovidRepository();
  });

  group('GetGlobalData', (){
    final global = Global(cases: 187642740, recovered: 4049289, deaths: 171597756);

    blocTest<GlobalBloc, GlobalState>(
      'emits success states',
      build: () {
        when(mockCovidRepository.fetchGlobalData)
            .thenAnswer((realInvocation) async => global);
        return GlobalBloc(mockCovidRepository);
      },
      act: (bloc) => bloc.add(GetGlobalData()),
      expect: () => [
        GlobalLoading(),
        GlobalDataLoaded(global),
      ],
    );

    blocTest<GlobalBloc, GlobalState>(
      'emits error states',
      build: () {
        when(mockCovidRepository.fetchGlobalData)
            .thenThrow(SocketException);
        return GlobalBloc(mockCovidRepository);
      },
      act: (bloc) => bloc.add(GetGlobalData()),
      expect: () => [
        GlobalLoading(),
        GlobalError('No Internet'),
      ],
    );


  });


}